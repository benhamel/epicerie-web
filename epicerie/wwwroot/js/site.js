﻿// Fichier pour le code générique au travers des pages
$(window).scroll(enteteTable);

function enteteTable() {
    let scroll = $(window).scrollTop();
    let anchor_top = $(".avecEntete").offset().top;
    //let anchor_bottom = $("#bottom_anchor").offset().top;
    //if (scroll > anchor_top && scroll < anchor_bottom) {
    if (scroll > anchor_top) {
        clone_table = $("#clone");
        if (clone_table.length == 0) {
            clone_table = $(".avecEntete").clone();
            clone_table.attr('id', 'clone');
            clone_table.css({
                position: 'fixed',
                'pointer-events': 'none',
                top: "50px"
            });
            clone_table.width($(".avecEntete").width());
            $("#table-container").append(clone_table);
            $("#clone").css({ visibility: 'hidden' });
            $("#clone thead").css({ 'visibility': 'visible', 'pointer-events': 'auto' });
        }
    } else {
        $("#clone").remove();
    }
}
